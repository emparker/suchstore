package com.suchcrowd.suchstore.message

/**
  * Created by emily on 5/3/16.
  */
case class SetRequest(key: String, value: Object)
