package com.suchcrowd.suchstore.actor

import akka.actor.Actor
import akka.actor.Actor.Receive
import akka.event.Logging
import com.suchcrowd.suchstore.message.SetRequest

import scala.collection.mutable.HashMap

/**
  * Created by emily on 5/3/16.
  */
class SuchStoreDb extends Actor{

  val map = new HashMap[String, Object]
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case SetRequest(key, value) => {
      log.info(s"received SetRequest - key: $key value: $value")
      map.put(key, value)
    }
    case o => log.info(s"received unknown message: $o");
  }
}
