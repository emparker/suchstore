package com.suchcrowd.suchstore.actor

import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import com.suchcrowd.suchstore.message.SetRequest
import org.scalatest._

/**
  * Created by emily on 5/3/16.
  */
class SuchStoreDbSpec extends FunSpec with Matchers with BeforeAndAfterEach{

  implicit val system = ActorSystem()

  describe("SuchStoreDb") {
    describe("given SetRequest") {
      it("should place key/value into map"){
        val actorRef = TestActorRef(new SuchStoreDb)
        actorRef ! SetRequest("key", "value")
        val db = actorRef.underlyingActor
        db.map.get("key") shouldEqual Some("value")
      }
    }
  }

}
